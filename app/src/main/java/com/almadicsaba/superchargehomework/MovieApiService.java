package com.almadicsaba.superchargehomework;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.Call;
import app.movie.tutorial.com.model.MovieResponse;

/**
 * Created by Csaba on 2018. 03. 10..
 */

// Interface which contains the methods to query the API
public interface MovieApiService {

    @GET("movie/{id}")
    Call<MovieResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);


}