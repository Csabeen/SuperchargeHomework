package com.almadicsaba.superchargehomework;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String baseUrl = "http://api.themoviedb.org/3/";
    private final String apiKey = "43a7ea280d085bd0376e108680615c7f";
    private Retrofit myRetrofit;
    private MovieApiService movieApiService;

    private RecyclerView recyclerView = null;
    private final String className = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // recyclerView initialization
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        RetrofitInitialization();
    }

    //method for retrofit initialization
    protected void RetrofitInitialization(){
        myRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    movieApiService = myRetrofit.create(MovieApiService.class);
}

//method to search a movie by ID
    public MovieResponse getMovieById(int id){

        Call<MovieResponse> call = movieApiService.getMovieDetails(id, apiKey);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                List<Movie> movies = response.body().getResults();
                recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });

    }

}
